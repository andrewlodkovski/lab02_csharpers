﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab02_CSharpers
{
    public partial class MainPage : ContentPage
    {

        public Country[] countries =
        {
            new Country("Australia", "Australia: Sydney museum, Uluru, The Big Merino, The Great Barrier Reef and much more", "australia_flag.png", "aus_sydney.jpg","aus_uluru.jpg","aus_cow.jpg","aus_reef.jpg"),
            new Country("Brazil", "Brazil: Rio de Janeiro, Iguazu Falls, Pampulha, Saint Francis Church and much more", "brazil_flag.png", "br_cristo.jpg", "br_falls.jpg", "br_art.jpg", "br_church.jpg"),
            new Country("Italy", "Italy: Venice, Coliseum, Trevi Fountain, Cathedral Santa Maria del Fiore and much more", "italy_flag.png", "italy_venice.jpg", "italy_col.jpg", "italy_palace.jpg", "italy_cath.jpg"),
            new Country("South Korea", "South Korea: Seoraksan National Park, Busan, Suwon, Seoul and much more", "sk_flag.png", "sk_park.jpg", "sk_busan.jpg", "sk_suwon.jpg", "sk_seoul.jpg"),
            new Country("United States", "The United States: Grand Canyon, San Diego, Yellowstone, Yosemite and much more", "us_flag.png", "us_gc.jpg", "us_sd.jpg", "us_yellowstone.jpg", "us_yosemite.jpg")
    };

        public MainPage()
        {
            InitializeComponent();

            ObservableCollection<Country> listOfCountries = new ObservableCollection<Country>(countries);
            CountriesList.ItemsSource = listOfCountries;
        }

        public void HandleRefreshing(object sender, EventArgs e)
        {
            Random color = new Random();
            BackgroundColor = new Color(color.NextDouble(), color.NextDouble(), color.NextDouble());
            CountriesList.IsRefreshing = false;
        }

        public async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            Country c = (Country) args.Item;
            if (c.Title == "Australia")
            {
                await Navigation.PushAsync(new JerrysAdditionalPage());
            }
            else if (c.Title == "Italy")
            {
                await Navigation.PushAsync(new Italy());
            }
            else if (c.Title == "South Korea")
            {
                await Navigation.PushAsync(new Yoojin_page());
            }
            else if (c.Title == "United States")
            {
                await Navigation.PushAsync(new Rachel_Page_Addition());
            }
        }
    }
}
