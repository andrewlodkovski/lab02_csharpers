﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lab02_CSharpers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Italy : ContentPage
    {
        public Italy()
        {
            InitializeComponent();
        }
    }
}