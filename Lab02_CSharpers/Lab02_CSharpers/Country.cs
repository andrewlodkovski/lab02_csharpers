﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab02_CSharpers
{
    public class Country
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Flag { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string Img4 { get; set; }


        public Country(string title, string description, string flag, string img1, string img2, string img3, string img4)
        {
            Title = title;
            Description = description;
            Flag = flag;
            Img1 = img1;
            Img2 = img2;
            Img3 = img3;
            Img4 = img4;
        }
    }
}
